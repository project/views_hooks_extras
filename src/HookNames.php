<?php

namespace Drupal\views_hooks_extras;

use Drupal\views\ViewExecutable;

/**
 * Helps to generate invokable hook names for the module.
 */
class HookNames {

  /**
   * Generates the extras names for a given hook/view/display.
   *
   * This is useful when the parent views hook doesn't provide
   * a display_id argument, but we might be able to infer it.
   *
   * @param string $function_name
   *   The local function whose hook names are being generated.
   * @param ViewExecutable $view
   *   The view.
   *
   * @return array
   *   List of invokable hook names.
   */
  public static function generate(string $function_name, ViewExecutable $view): array {
    return self::generateByString($function_name, $view->id(), $view->current_display);
  }

  /**
   * Generates the extras names for a given hook/view/display.
   *
   * @param string $function_name
   *   The local function whose hook names are being generated.
   * @param string $view_id
   *   The view.
   * @param string|null $display_id
   *   (optional) The view display id. Not all views give easy access
   *   to the appropriate display_id.
   *
   * @return array
   *   List of invokable hook names.
   */
  public static function generateByString(string $function_name, string $view_id, ?string $display_id = NULL): array {
    $base = str_replace('views_hooks_extras_', '', $function_name);

    // All views hooks begin with hook_field_views or hook_views,
    // so it's safe to assume that if it doesn't start with 'field_views'
    // then it starts with 'views'.
    $hook_prefix = strpos($base, 'field_views') === 0
      ? 'field_views' : 'views';
    $hook_suffix = str_replace("{$hook_prefix}_", '', $base);

    $variants = [$view_id];
    if ($display_id) {
      $display_id_snake = Strings::toSnakeCase($display_id);
      $variants[] = "{$view_id}__{$display_id_snake}";
    }

    // Ideally we could use an arrow function, but that builds in
    // a pointless dependency on 7.4+.
    return array_map(function (string $variant) use ($hook_prefix, $hook_suffix) {
      return "extra_{$hook_prefix}_{$variant}_{$hook_suffix}";
    }, $variants);
  }

}

<?php

namespace Drupal\views_hooks_extras;

class Strings {

  /**
   * Converts a given string to camelCase.
   *
   * @param string $string
   *   The string being manipulated.
   *
   * @return string
   *   The updated string.
   */
  public static function toCamelCase(string $string): string {
    $unpunctuated = self::punctuationToSpaces($string);
    $str = ucwords($unpunctuated);
    $str = str_replace(' ', '', $str);
    return lcfirst($str);
  }

  /**
   * Converts a given string to snake-case.
   *
   * @param string $string
   *   The string being manipulated.
   *
   * @return string
   *   The updated string.
   */
  public static function toSnakeCase(string $string): string {
    $unpunctuated = self::punctuationToSpaces($string);
    $lower = strtolower($unpunctuated);
    return str_replace(' ', '_', $lower);
  }

  /**
   * Replaces punctuation with spaces in given string.
   *
   * @param string $string
   *   The string being manipulated.
   *
   * @return string
   *   The updated string.
   */
  public static function punctuationToSpaces(string $string): string {
    // From https://stackoverflow.com/questions/5233734/how-to-strip-punctuation-in-php
    return preg_replace("/(?![.=$'€%-])\p{P}/u", " ", $string);
  }

  /**
   * Trims a string to max length, respecting sentences.
   *
   * @param string $string
   *   The string.
   * @param int $max_length
   *   (optional) The desired max length of the trimmed string.
   *
   * @return string
   *   The trimmed string.
   */
  public static function trimString(string $string, int $max_length = 220): string {
    // Based on https://stackoverflow.com/questions/9870974/replace-nbsp-characters-that-are-hidden-in-text/9871045
    $string = htmlentities($string, NULL, 'utf-8');
    $string = str_replace("&nbsp;", " ", $string);
    $string = html_entity_decode($string);

    if (strlen($string) <= 220) {
      return $string;
    }

    $str_short = substr($string, 0, $max_length);
    $str_short = explode('. ', $str_short);
    // Remove last sentence from array.
    array_pop($str_short);
    $str_short = implode('. ', $str_short) . ".";
    return $str_short;

  }

}

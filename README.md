CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------

This module extends Views' native hooks to more specifically target particular views and their displays. This can help
to aid in function specificity and avoid function calls with tons of conditional logic just to delineate with views are
being targeted.


REQUIREMENTS
------------

This module only requires that Views be enabled.


INSTALLATION
------------

Install the Views Hooks Extras module as you would normally install a contributed Drupal module.
Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

1. Navigate to Administration > Extend and enable the module.
2. ???
3. Profit.

There are conflicting conventions take by core for extending hooks. Most take the form of `hook_form_FORM_ID_alter`, but
we also have `hook_preprocess_HOOK`. The latter is more of an outlier, so this module takes the first approach as it
aligns more closely with the majority of hook implementations:

- `hook_extra_viewsPrefix_VIEW_ID_viewsSuffix`
- `hook_extra_viewsPrefix_VIEW_ID__DISPLAY_ID_viewsSuffix`

The extra_ prefix was added just to ensure that there won't ever be a conflict with potential core implementations of
the same concept.

If you wanted to implement `hook_views_pre_view` for a view with ID favorite_fruits, you would
implement `mymodule_extra_views_favorite_fruits_pre_view`. If you specifically wanted to target the page_1 display,
you'd implement `mymodule_extra_views_favorite_fruits__page_1_pre_view`.

It's important to note that the `DISPLAY_ID` portion takes on the snake case version of the actual display ID. That is
to say, if you happened to name your view display to be `page-1`, the hook would need to use `page_1`.

At time of writing, the following views hooks are currently supported:

- hook_views_query_substitutions
- hook_views_pre_view
- hook_views_pre_build
- hook_views_post_build
- hook_views_pre_execute
- hook_views_pre_render
- hook_views_post_render
- hook_views_query_alter
- hook_views_preview_info_alter

Logic already exists to support the `field_views` and `plugin` hooks, but we aren't sure whether there's a way to parse
a particular view or display ID from the available parameters.

MAINTAINERS
-----------

* Matthew Weiner - https://www.drupal.org/u/mrweiner

Supporting organization:

* Flat 9, Inc. - https://www.drupal.org/flat-9

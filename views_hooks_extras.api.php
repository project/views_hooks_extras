<?php

/**
 * @file
 * Describes hooks and plugins provided by the views_hooks_extras module.
 */

use Drupal\views\Plugin\views\cache\CachePluginBase;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ViewExecutable;

/**
 * Executes hook_views_query_substitutions for a specific View ID.
 */
function hook_extra_views_VIEW_ID_query_substitutions(ViewExecutable $view) {
}

/**
 * Executes hook_views_query_substitutions for a specific View ID and Display ID.
 */
function hook_extra_views_VIEW_ID__DISPLAY_ID_query_substitutions(ViewExecutable $view) {
}

/**
 * Executes hook_views_pre_view for a specific View ID.
 */
function hook_extra_views_VIEW_ID_pre_view(ViewExecutable $view, $display_id, array &$args) {
}

/**
 * Executes hook_views_pre_view for a specific View ID and Display ID.
 */
function hook_extra_views_VIEW_ID__DISPLAY_ID_pre_view(ViewExecutable $view, $display_id, array &$args) {
}

/**
 * Executes hook_views_pre_build for a specific View ID.
 */
function hook_extra_views_VIEW_ID_pre_build(ViewExecutable $view) {
}

/**
 * Executes hook_views_pre_build for a specific View ID and Display ID.
 */
function hook_extra_views_VIEW_ID__DISPLAY_ID_pre_build(ViewExecutable $view) {
}

/**
 * Executes hook_views_pose_build for a specific View ID.
 */
function hook_extra_views_VIEW_ID_post_build(ViewExecutable $view) {
}

/**
 * Executes hook_views_post_build for a specific View ID and Display ID.
 */
function hook_extra_views_VIEW_ID__DISPLAY_ID_post_build(ViewExecutable $view) {
}

/**
 * Executes hook_views_pre_execute for a specific View ID.
 */
function hook_extra_views_VIEW_ID_pre_execute(ViewExecutable $view) {
}

/**
 * Executes hook_views_pre_execute for a specific View ID and Display ID.
 */
function hook_extra_views_VIEW_ID__DISPLAY_ID_pre_execute(ViewExecutable $view) {
}

/**
 * Executes hook_views_pre_render for a specific View ID.
 */
function hook_extra_views_VIEW_ID_pre_render(ViewExecutable $view) {
}

/**
 * Executes hook_views_pre_render for a specific View ID and Display ID.
 */
function hook_extra_views_VIEW_ID__DISPLAY_ID_pre_render(ViewExecutable $view) {
}

/**
 * Executes hook_views_post_render for a specific View ID.
 */
function hook_extra_views_VIEW_ID_post_render(ViewExecutable $view, array &$output, CachePluginBase $cache) {
}

/**
 * Executes hook_views_post_render for a specific View ID and Display ID.
 */
function hook_extra_views_VIEW_ID__DISPLAY_ID_post_render(ViewExecutable $view, array &$output, CachePluginBase $cache) {
}

/**
 * Executes hook_views_query_alter for a specific View ID.
 */
function hook_extra_views_VIEW_ID_query_alter(ViewExecutable $view, QueryPluginBase $query) {
}

/**
 * Executes hook_views_query_alter for a specific View ID and Display ID.
 */
function hook_extra_views_VIEW_ID__DISPLAY_ID_query_alter(ViewExecutable $view, QueryPluginBase $query) {
}

/**
 * Executes hook_views_preview_info_alter for a specific View ID.
 */
function hook_extra_views_VIEW_ID_preview_info_alter(array &$rows, ViewExecutable $view) {
}

/**
 * Executes hook_views_preview_info_alter for a specific View ID.
 */
function hook_extra_views_VIEW_ID__DISPLAY_ID_preview_info_alter(array &$rows, ViewExecutable $view) {
}
